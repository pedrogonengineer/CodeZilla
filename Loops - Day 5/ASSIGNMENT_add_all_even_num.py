#Write your code below this row 👇

even_check = 0
total = 0

for x in range(0,101):
    even_check = x%2
    if even_check == 0:
        total += x

print(total)

# We could have also used the range function by jumping my 2 range(0,101,2)