#Write your code below this row 👇
# Problem statement:
# If a number is divisible by 3 print Fizz
# If a number is divisible by 5 print Buzz
# If a number is divisible by both 3 & 5, print FizzBuzz
# Run numbers from 1 - 100 

for x in range(1,101):
    if x%3 == 0 and x%5 == 0:
        print("FizzBuzz")
    elif x%3 ==0:
        print("Fizz")
    elif x%5 == 0:
        print("Buzz")
    else:
        print(x)
        