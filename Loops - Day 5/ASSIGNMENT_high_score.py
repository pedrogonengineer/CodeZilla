# 🚨 Don't change the code below 👇
student_scores = input("Input a list of student scores ").split()
for n in range(0, len(student_scores)):
  student_scores[n] = int(student_scores[n])
print(student_scores)
# 🚨 Don't change the code above 👆

#Write your code below this row 👇
high_score = student_scores[0]
count = 1

for x in range(1,len(student_scores)):
    if count < len(student_scores):
        if high_score < student_scores[count]:
            high_score = student_scores[count]
            count += 1
        else:
            count +=1

print(f"The highest score in the class is: {high_score}")